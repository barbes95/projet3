package controllers;
import java.util.Properties;
import model.LoadProperties;


public class ChargementFichier {

	private static final String filename = "Jeu.properties";
	private String valeurCle;
	private boolean valeurModeDev;


	public void properties(String key) {
		try {
			Properties prop = LoadProperties.load(filename);
			setValeurCle(prop.getProperty(key));

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void propertiesModeDev(String key) {
		try {
			Properties prop = LoadProperties.load(filename);
			setValeurModeDev(Boolean.parseBoolean(prop.getProperty(key)));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public String getValeurCle() {
		return valeurCle;
	}

	private void setValeurCle(String valeurCle) {
		this.valeurCle = valeurCle;
	}

	public boolean isValeurModeDev() {
		return valeurModeDev;
	}

	public void setValeurModeDev(boolean valeurModeDev) {
		this.valeurModeDev = valeurModeDev;
	}
}
