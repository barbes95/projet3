package controllers;

import org.apache.log4j.Logger;

import enumeration.Choix;
import model.Joueur;
import services.ChargementFichier;
import views.AffichageJeu;

abstract class Jeu {
	static ChargementFichier chargementProprietes = new ChargementFichier();
	
	private static int nombreChiffre;
	private int idJeu;
	
	
	private String combinaison ="";
	private static String clePlusMoins = "plusmoins.nombreChiffre";
	private static String cleMasterMind = "mastermind.nombreChiffre";
	private static String cleModeDev = "jeu.modeDeveloppeur";
	private static String cleTentativeMaxMM = "mastermind.tentativeMax";
	private static String cleTentativeMaxPM = "plusmoins.tentativeMax";
	
	private boolean finTraitement = false;
	protected boolean sortieTentative = false;
	private static boolean modeDev;
	protected boolean tokenDebug;
	
	private Joueur joueur;
	private Joueur ordinateur;
	
	abstract public void modeChallenger();
	abstract public void modeDefenseur();
	abstract public void modeDuel();
	protected abstract  AffichageJeu getAffichage();
	protected abstract void tableauChiffreCombinaison(); // Generation nombre aléatoire
	private static Logger loggerJeu = Logger.getLogger(Jeu.class);

	public Jeu(Joueur joueur, Joueur ordinateur) {
		
		this.setJoueur(joueur);
		this.setOrdinateur(ordinateur);
		joueur.setTentativeMaxMM(getCleTentativeMaxMM());
		joueur.setTentativeMaxPM(getCleTentativeMaxPM());
		joueur.setTentative(0);
		setFinTraitement(false);
	}
	
    public long [] renverseTableauxEntiers (long [] t)
    {
    	loggerJeu.debug("Inversion du tableau");
        int n = t.length;
        long [] rev;
        rev = new long [n];
        // On renverse le tableau
        for (int i=0; i<= n-1; i=i+1)
            rev[i] = t[n-1-i];

        return rev;
    }
    
	public long[] initTableauRecherche(int longueurNombre) {
		loggerJeu.debug("Initialisation du tableau de recherche de l'ordinateur à 5");
		long [] tableauIni;
		tableauIni = new long [longueurNombre];
		for(int i = 0; i < getNombreChiffre(); i++) {
			tableauIni[i] = 5;
		}
		return tableauIni;
	}
	
    

	
    protected Boolean startPremierJoueur()
    {	
    	loggerJeu.debug("Choix du premier joueur selon l'entrée de l'utilisateur");
        Boolean playerStart = Choix.isPlayerStart(joueur.entreeUtilisateurString());
        return playerStart;
    }
    

	protected String getCombinaison() {
		return combinaison;
	}
	
	protected void setCombinaison(String combinaison) {
		this.combinaison = combinaison;
	}
	protected int getNombreChiffre() {
		return (int) nombreChiffre;
	}

	protected boolean isFinTraitement() {
		return finTraitement;
	}
	protected void setFinTraitement(boolean finTraitement) {
		this.finTraitement = finTraitement;
	}

	protected Joueur getJoueur() {
		return joueur;
	}

	protected void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	protected static void setNombreChiffre(String cle) {
		chargementProprietes.properties(cle);
		nombreChiffre = Integer.parseInt(chargementProprietes.getValeurCle());
	}
	
	protected int getIdJeu() {
		return idJeu;
	}
	
	protected void setIdJeu(long saisieUtilisateur) {
		this.idJeu = (int)saisieUtilisateur;
	}
	protected Joueur getOrdinateur() {
		return ordinateur;
	}
	protected void setOrdinateur(Joueur ordinateur) {
		this.ordinateur = ordinateur;
	}
	public static String getClePlusMoins() {
		return clePlusMoins;
	}
	public static String getCleMasterMind() {
		return cleMasterMind;
	}
	protected boolean isSortieTentative() {
		return sortieTentative;
	}

	protected void setSortieTentative(boolean sortieTentative) {
		this.sortieTentative = sortieTentative;
	}
	public static String getCleModeDev() {
		return cleModeDev;
	}
	public boolean isModeDev() {
		return modeDev;
	}
	
	public void setModeDev(boolean modeDev) {
		Jeu.modeDev = modeDev;
	}
	public static void setModeDevCle(String cle) {
		chargementProprietes.propertiesModeDev(cle);
		modeDev = chargementProprietes.isValeurModeDev();
	}
	public static String getCleTentativeMaxMM() {
		return cleTentativeMaxMM;
	}
	public static void setCleTentativeMaxMM(String cleTentativeMaxMM) {
		Jeu.cleTentativeMaxMM = cleTentativeMaxMM;
	}
	public static String getCleTentativeMaxPM() {
		return cleTentativeMaxPM;
	}
	public static void setCleTentativeMaxPM(String cleTentativeMaxPM) {
		Jeu.cleTentativeMaxPM = cleTentativeMaxPM;
	}
	public boolean isTokenDebug() {
		return tokenDebug;
	}
	public void setTokenDebug(boolean tokenDebug) {
		this.tokenDebug = tokenDebug;
	}
}
