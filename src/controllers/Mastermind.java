package controllers;

import java.util.Random;
import org.apache.log4j.Logger;


import model.Joueur;

import views.AffichageMasterMind;

public class Mastermind extends Jeu{

	private AffichageMasterMind affichage = new AffichageMasterMind();
	private static Logger loggerMasterMind = Logger.getLogger(Mastermind.class);
	private long combinaisonSecrete[] = new long[getNombreChiffre()];
	private long ordinateurRechercheCombi[ ] = new long[getNombreChiffre()];
	private long tableauRechercheReponse[ ] = new long[getNombreChiffre()];
	private long nombreUtilisateur[] = new long[getNombreChiffre()];
	private boolean [] valeurTraitee = new boolean[getNombreChiffre()];

	private int chiffreBienPlace = 0;
	private int chiffrePresentMalPlace = 0;

	int varIncr = 0;


	private boolean tokenDuel = false;

	public Mastermind(Joueur joueur, Joueur ordinateur) {
		super(joueur, ordinateur);
		joueur.setTentative(0);
		ordinateur.setTentative(0);
		ordinateurRechercheCombi = initTableauRecherche(getNombreChiffre());
	}

	public void modeChallenger() {
		if(getJoueur().getTentative() > (getJoueur().getTentativeMaxMM()-1)) {
			getAffichage().tentativeMaxJoueur(getJoueur().getTentativeMaxMM());
			getAffichage().affichageEnteteSolution();
			getAffichage().affichageSolution(getCombinaisonSecrete(), getNombreChiffre());
			setSortieTentative(true);
			return;
		}
		getAffichage().tentative(getJoueur().getTentative()+1);
		if(getJoueur().saisieValide(getNombreChiffre())) {
			reverseNombre(getJoueur().getTmp());
			setFinTraitement(verificationChiffrePresent(this.nombreUtilisateur));
			getAffichage().challengerPlace(getChiffreBienPlace());
			getAffichage().challengerPresent(getChiffrePresentMalPlace());
		}
		else {
			affichage.erreurLongueurCombinaison(getNombreChiffre());
			modeChallenger();
		}
	}

	public void modeDefenseur() {
		boolean valeurMinOuMax = initialisationTableauOrdinateurMasterMind();
		tokenDuel = true;

		if(getOrdinateur().saisieValide(getNombreChiffre())) {
			reverseNombre(getOrdinateur().getTmp());
			while(!isFinTraitement()) {
				getOrdinateur().setTentativeIncr();
				getAffichage().tentative(getOrdinateur().getTentative());
				getAffichage().affichageSolution(ordinateurRechercheCombi, getNombreChiffre());
				setFinTraitement(verificationChiffrePresent(ordinateurRechercheCombi));
				getAffichage().challengerPlace(getChiffreBienPlace());
				getAffichage().challengerPresent(getChiffrePresentMalPlace());

				if(getOrdinateur().getTentative() > getOrdinateur().getTentativeMaxMM()-1) {
					getAffichage().tentativeMaxAtteintes();
					getAffichage().affichageEnteteSolution();
					getAffichage().affichageSolution(getCombinaisonSecrete(), getNombreChiffre());
					setFinTraitement(true);
					return;
				}
				else if(tableauEgaux()){
					getOrdinateur().setTentativeIncr();
					getAffichage().tentative(getOrdinateur().getTentative());
					getAffichage().affichageSolution(tableauRechercheReponse, getNombreChiffre());
					setFinTraitement(verificationChiffrePresent(tableauRechercheReponse));
					getAffichage().challengerPlace(getChiffreBienPlace());
					getAffichage().challengerPresent(getChiffrePresentMalPlace());
					getAffichage().affichageWinOrdinateur();
					getAffichage().affichageSolution(tableauRechercheReponse, getNombreChiffre());

					setFinTraitement(true);
					return;
				}
				else {
					tableauRechercheCombinaison(valeurMinOuMax);
				}	

			}
		}
		else {
			affichage.erreurLongueurCombinaison(getNombreChiffre());
			modeDefenseur();
		}
	}

	public void modeDuel() {
		boolean token = true;
		boolean saisieOk = false;
		affichage.debutDuelPileOuFace();

		Boolean premierJoueur = startPremierJoueur();

		while(premierJoueur == null)
		{
			affichage.erreurSaisieSimple();
			premierJoueur = startPremierJoueur();
		}

		token = (premierJoueur == true);
		boolean valeurMinOuMax = initialisationTableauOrdinateurMasterMind();

		while(!isFinTraitement())
		{
			if(token == true)
			{
				tokenDuel = false;
				getJoueur().setTentativeIncr();
				getAffichage().tourJoueur();
				getAffichage().affichageTentative(getJoueur().getTentative());
				getAffichage().saisieCombinaison(getNombreChiffre());
				saisieOk = getJoueur().saisieValide(getNombreChiffre());

				while(!saisieOk) {
					getAffichage().erreurLongueurCombinaison(getNombreChiffre());
					saisieOk = getJoueur().saisieValide(getNombreChiffre());
				}
				reverseNombre(getJoueur().getTmp());
				setFinTraitement(verificationChiffrePresent(this.nombreUtilisateur));
				getAffichage().challengerPlace(getChiffreBienPlace());
				getAffichage().challengerPresent(getChiffrePresentMalPlace());


				if(isFinTraitement())
				{
					getAffichage().victoireJoueur(getJoueur().getTentative());
					break;
				}
				token = false;
			}
			else 
			{				

				tokenDuel = true;
				getOrdinateur().setTentativeIncr();
				getAffichage().tourOrdinateur();
				getAffichage().tentative(getOrdinateur().getTentative());	

				if(getOrdinateur().getTentative() > getOrdinateur().getTentativeMaxMM()-1) {
					getAffichage().tentativeMaxAtteintes();
					getAffichage().affichageEnteteSolution();
					getAffichage().affichageSolution(getCombinaisonSecrete(), getNombreChiffre());
					setFinTraitement(true);
					return;

				}

				if(tableauEgaux()) {
					getAffichage().affichageSolution(tableauRechercheReponse, getNombreChiffre());
					setFinTraitement(verificationChiffrePresent(tableauRechercheReponse));
					getAffichage().challengerPlace(getChiffreBienPlace());
					getAffichage().challengerPresent(getChiffrePresentMalPlace());
					getAffichage().victoireOrdinateur(getOrdinateur().getTentative());
					getAffichage().affichageWinOrdinateur();
					getAffichage().affichageSolution(tableauRechercheReponse, getNombreChiffre());
					setFinTraitement(true);
					return;
				}
				else {
					getAffichage().affichageSolution(ordinateurRechercheCombi, getNombreChiffre());
					setFinTraitement(verificationChiffrePresent(ordinateurRechercheCombi));
					getAffichage().challengerPlace(getChiffreBienPlace());
					getAffichage().challengerPresent(getChiffrePresentMalPlace());
					tableauRechercheCombinaison(valeurMinOuMax);
				}

				token = true;
			}
		}
	}



	/**
	 * @param nombreUtilisateur
	 * 
	 * Permet de remettre le nombre dans le bon sens
	 * Si c'est à l'ordinateur de trouver la réponse ou en mode duel, cela devient la combinaison secrete.
	 */
	public void reverseNombre(long nombreUtilisateur)
	{
		for(int i = 0; i < getNombreChiffre(); i++)
		{
			this.nombreUtilisateur[i] = nombreUtilisateur % 10;
			nombreUtilisateur /= 10;
		}
		this.nombreUtilisateur = renverseTableauxEntiers(this.nombreUtilisateur);
		if(getOrdinateur().getIdentifiant() == 2 && tokenDuel) {
			setCombinaisonSecrete(this.nombreUtilisateur);
		}	
	}

	/**
	 * @param combinaisonUtilisateur
	 * @return Retourne vrai si les tout les chiffres sont présent sinon faux
	 * 
	 * Permet dans un premier temps de vérifier combien de chiffre sont bien placés puis de 
	 * vérifier les présents mais pas à la bonne place
	 */
	private boolean verificationChiffrePresent(long combinaisonUtilisateur[])
	{

		setChiffreBienPlace(0);
		setChiffrePresentMalPlace(0);
		
		if(isModeDev() && isTokenDebug()) {
			loggerMasterMind.trace("Verification des chiffres présents");
		}
		for(int i = 0; i < getNombreChiffre(); i++) {
			if(combinaisonUtilisateur[i] == getCombinaisonSecrete()[i]) {
				if(getOrdinateur().getIdentifiant() == ScenarioJeu.JOUEUR_ORDINATEUR && tokenDuel) {
					tableauRechercheReponse[varIncr] = combinaisonUtilisateur[i];
					varIncr++;
				}
				setChiffreBienPlace(getChiffreBienPlace()+1);
				valeurTraitee[i] = true;
			}
			else {
				valeurTraitee[i] = false;
			}
		}
		if(getChiffreBienPlace() == getNombreChiffre()) {
			if(isModeDev() && isTokenDebug()) {
				loggerMasterMind.trace("Tout les chiffres sont présents et bien placés");
			}
			return true;
		}

		if(isModeDev() && isTokenDebug()) {
			loggerMasterMind.trace("Verification des chiffres présents");
		}

		for(int i = 0; i < getNombreChiffre(); i++) {
			if(combinaisonUtilisateur[i] != getCombinaisonSecrete()[i]) {
				int j = 0;
				boolean mauvaisePlacePresent = false;

				while((j < getNombreChiffre()) && !mauvaisePlacePresent) {
					if(!valeurTraitee[j] && (combinaisonUtilisateur[i] == getCombinaisonSecrete()[j])) {
						setChiffrePresentMalPlace(getChiffrePresentMalPlace()+1);
						valeurTraitee[j] = true;
						mauvaisePlacePresent = true;
					}
					j++;
				}
			}
		}
		return false;
	}

	/**
	 * @return Retourne le booleen qui permet de savoir si on a commencé à 0 ou à 9
	 * 
	 * Permet d'initialiser aléatoirement le tableau de recherche de l'ordinateur avec tout à 0 ou tout à 9
	 */
	public boolean initialisationTableauOrdinateurMasterMind()
	{
		boolean boolRandom = Math.random() < 0.5;
		if(isModeDev() && isTokenDebug()) {
			loggerMasterMind.trace("Initialisation du tableau de recherche de l'ordinateur selon un boolean à 0 ou 9");
		}
		if (boolRandom) {
			for(int i = 0; i < getNombreChiffre(); i++)
			{
				this.ordinateurRechercheCombi[i] = 0;
			}
			if(isModeDev() && isTokenDebug()) {
				loggerMasterMind.trace("Tableau rempli avec des 0");
			}
		} else {
			for(int i = 0; i < getNombreChiffre(); i++)
			{
				this.ordinateurRechercheCombi[i] = 9;
			}
			if(isModeDev() && isTokenDebug()) {
				loggerMasterMind.trace("Tableau rempli avec des 9");
			}
		}
		return boolRandom;
	}

	/**
	 * @return Renvoi vrai si les tableaux sont égaux sinon faux
	 * 
	 * Vérifie si le tableau de recherche et la combinaison sont égaux
	 * Si ils sont égaux, chiffreBienPlace passe à la valeur de nombre chiffre
	 */
	public boolean tableauEgaux() {
		int i=0;

		while (i<getNombreChiffre()) { 
			if (tableauRechercheReponse[i] != getCombinaisonSecrete()[i]) break;
			i++;
		}
		if (i==getNombreChiffre()) { 
			setChiffreBienPlace(getNombreChiffre());
			if(isModeDev() && isTokenDebug()) {
				loggerMasterMind.trace("Verification chiffres bien placé avec le nombre de chiffre total dans la combinaison");
			}
			return true;
		}
		else {
			return false;
		}

	}

	/** 
	 * Permet d'initialiser une combinaison aléatoire 
	 * Si le mode debug est activé dans le fichier de configuration, on affiche la combinaison
	 **/
	protected void tableauChiffreCombinaison() {
		Random rand = new Random();
		for(int i = 0; i < getNombreChiffre(); i++)
		{
			this.combinaisonSecrete[i] = rand.nextInt(9);
			while(this.combinaisonSecrete[i] < 1 && i == 0) {
				this.combinaisonSecrete[i] = rand.nextInt(9);
			}
		}
		if(isModeDev() && isTokenDebug()) {
			loggerMasterMind.debug("La combinaison secréte est : " + getCombinaison());
		}
	}

	/** 
	 * Permet d'incrémenter ou décrémenter les chiffres contenu dans le tableau pour la recherche de la combinaison
	 * par l'ordinateur 
	 **/
	private void tableauRechercheCombinaison(boolean debutTableau) {

		for(int j = 0; j < getNombreChiffre(); j++)
		{
			if(ordinateurRechercheCombi[j] < 0 || ordinateurRechercheCombi[j] > 9) {
				rotationCombinaison();
			}
			else {
				if (debutTableau) {
					ordinateurRechercheCombi[j]++;
				}
				else {
					ordinateurRechercheCombi[j]--;
				}
			}
		}
		if(isModeDev() && isTokenDebug()) {
			loggerMasterMind.debug("Incrémentation ou décrémentation du tableau de recherche selon le boolean");
		}
	}

	private void rotationCombinaison() {
		//tableauRechercheReponse
	}
	
	public AffichageMasterMind getAffichage() {
		return affichage;
	}

	private long[] getCombinaisonSecrete() {
		return combinaisonSecrete;
	}

	private void setCombinaisonSecrete(long combinaisonSecrete[]) {
		this.combinaisonSecrete = combinaisonSecrete;
	}

	public int getChiffreBienPlace() {
		return chiffreBienPlace;
	}

	public void setChiffreBienPlace(int chiffreBienPlace) {
		this.chiffreBienPlace = chiffreBienPlace;
	}

	public int getChiffrePresentMalPlace() {
		return chiffrePresentMalPlace;
	}

	public void setChiffrePresentMalPlace(int chiffrePresentMalPlace) {
		this.chiffrePresentMalPlace = chiffrePresentMalPlace;
	}

}