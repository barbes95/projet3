package controllers;

import enumeration.ChoixJeu;
import enumeration.ChoixMode;
import model.Joueur;
import views.AffichageJeu;
import views.AffichageUtilisateur;
import org.apache.log4j.Logger;


public class ScenarioJeu {

	private Joueur joueur1 = new Joueur();
	private Joueur joueur2 = new Joueur();

	static final int JOUEUR_HUMAIN = 1;
	static final int JOUEUR_ORDINATEUR = 2;
	static final Logger logger = Logger.getLogger(ScenarioJeu.class);

	public boolean choixJeuUtilisateur(int debug) {
		AffichageUtilisateur affichageMenu = new AffichageUtilisateur();
		long saisieUtilisateur = 0;

		affichageMenu.menuJeu();
		saisieUtilisateur = joueur1.entreeUtilisateur();

		Jeu.setModeDevCle(Jeu.getCleModeDev());
		ChoixJeu choixJeu = ChoixJeu.jeu(saisieUtilisateur);
		Jeu jeu = initialisationJeu(choixJeu);
		while (jeu == null) {
			if(ChoixJeu.QUITTER.equals(choixJeu)) {
				return false;
			}
			affichageMenu.erreurSaisie();
			affichageMenu.menuJeu();
			saisieUtilisateur = joueur1.entreeUtilisateur();
			choixJeu = ChoixJeu.jeu(saisieUtilisateur);
			jeu = initialisationJeu(choixJeu);
		}
		if(debug == 1) {
			jeu.setTokenDebug(true);
			jeu.setModeDev(true);
		}
		else
			jeu.setTokenDebug(false);

		jeu.setIdJeu(saisieUtilisateur);
		choixModeUtilisateur(jeu);

		return true;
	}

	private void choixModeUtilisateur(Jeu jeu) {
		AffichageJeu affichageMenu = jeu.getAffichage();
		long saisieUtilisateur = 0;
		affichageMenu.menuModeJeu();
		saisieUtilisateur = joueur1.entreeUtilisateur();

		ChoixMode choixM = ChoixMode.modeDeJeu(saisieUtilisateur);
		while (choixM == null) {
			affichageMenu.erreurSaisie();
			affichageMenu.menuModeJeu();
			saisieUtilisateur = joueur1.entreeUtilisateur();
			choixM = ChoixMode.modeDeJeu(saisieUtilisateur);	
		}
		preparationLancementJeu(jeu, choixM);
	}

	private void preparationLancementJeu(Jeu jeu, ChoixMode modeChoisi) {
		if(jeu.isModeDev() && jeu.isTokenDebug()) {
			logger.trace("Choix du mode de jeu selon le choix utilisateur");
		}

		switch (modeChoisi) {

		case MODECHALLENGER:
			if(jeu.isModeDev() && jeu.isTokenDebug()) {
				logger.trace("Mode Challenger : Attribution identifiant du joueur et lancement du jeu");
			}
			joueur1.setIdentifiant(JOUEUR_HUMAIN);
			jeu.tableauChiffreCombinaison();
			while(!jeu.isFinTraitement()) {
				jeu.modeChallenger();
				joueur1.setTentativeIncr();
				if(jeu.isSortieTentative()) {
					jeu.setFinTraitement(true);
				}
			}
			if(jeu.isSortieTentative()) {
				break;
			}
			jeu.getAffichage().affichageNombreTentative(joueur1.getTentative());
			break;

		case MODEDEFENSEUR:
			if(jeu.isModeDev() && jeu.isTokenDebug()) {
				logger.trace("Mode Defenseur : Attribution identifiant du joueur et lancement du jeu");
			}
			joueur2.setIdentifiant(JOUEUR_ORDINATEUR);
			jeu.getAffichage().saisieCombinaison(jeu.getNombreChiffre());			
			while(!jeu.isFinTraitement()) {
				joueur2.setTentativeIncr();
				jeu.modeDefenseur();
			}
			jeu.getAffichage().affichageNombreTentative(joueur2.getTentative());
			break;

		case MODEDUEL:
			if(jeu.isModeDev() && jeu.isTokenDebug()) {
				logger.trace("Mode Duel : Attribution des identifiants des joueurs et lancement du jeu");
			}
			joueur1.setIdentifiant(JOUEUR_HUMAIN);
			joueur2.setIdentifiant(JOUEUR_ORDINATEUR);

			jeu.tableauChiffreCombinaison();
			while(!jeu.isFinTraitement()) {
				jeu.modeDuel();
			}


			break;

		case RETOUR:
			break;

		default:
			break;
		}
	}

	private Jeu initialisationJeu(ChoixJeu jeuChoisi) {
		if(jeuChoisi == null) {
			return null;
		}
		switch (jeuChoisi) {
		case PLUSOUMOINS:
			Jeu.setNombreChiffre(Jeu.getClePlusMoins());
			return new PlusMoins(getJoueur(), getJoueur2());

		case MASTERMIND:
			Jeu.setNombreChiffre(Jeu.getCleMasterMind());
			return new Mastermind(getJoueur(), getJoueur2());

		default:
			return null;
		}
	}

	protected Joueur getJoueur() {
		return joueur1;
	}

	protected void setJoueur(Joueur joueur) {
		this.joueur1 = joueur;
	}

	protected Joueur getJoueur2() {
		return joueur2;
	}

	protected void setJoueur2(Joueur joueur2) {
		this.joueur2 = joueur2;
	}
}





