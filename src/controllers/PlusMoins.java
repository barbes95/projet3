package controllers;


import java.util.Random;
import views.AffichagePlusMoins;
import org.apache.log4j.Logger;

import model.Joueur;


public class PlusMoins extends Jeu{

	private static Logger loggerPlusMoins = Logger.getLogger(PlusMoins.class);
	private long nombreUtilisateur[ ] = new long[getNombreChiffre()];
	private long ordinateurRechercheCombi[] = new long[getNombreChiffre()];
	private long nombreSecret[] = new long[getNombreChiffre()];
	private int valeurChiffreOk = 0;
	private boolean valeurRetourFonction = false;
	private boolean token = true;
	private long longueurNombreUtilisateur;
	private long tmp;
	private AffichagePlusMoins affichage = new AffichagePlusMoins();
	
	PlusMoins(Joueur joueur, Joueur ordinateur){
		super(joueur , ordinateur);
		joueur.setTentative(0);
		ordinateur.setTentative(0);
		ordinateurRechercheCombi = initTableauRecherche(getNombreChiffre());
	}

	protected void tableauChiffreCombinaison() {
    	Random rand = new Random();
		for(int i = 0; i < getNombreChiffre(); i++)
		{
			nombreSecret[i] = rand.nextInt(9);
			while(this.nombreSecret[i] < 1 && i == 0) {
				this.nombreSecret[i] = rand.nextInt(9);
			}
			this.setCombinaison(this.getCombinaison() + this.nombreSecret[i]);
		}			
		if(isModeDev() && isTokenDebug()) {
			loggerPlusMoins.debug("La combinaison secréte est : " + Integer.parseInt(getCombinaison()));
		}
		System.out.println("");
	}
	
	/**
	 * @param nombreUtilisateur
	 * 
	 * Permet de remettre le nombre dans le bon sens
	 * Si c'est à l'ordinateur de trouver la réponse ou en mode duel, cela devient la combinaison secrete.
	 */
    public void reverseTabUtilisateur(long nombre)
    {
    	for(int i = 0; i < getNombreChiffre(); i++)
		{
			this.nombreUtilisateur[i] = nombre % 10;
			nombre /= 10;
		}
		if(isModeDev() && isTokenDebug()) {
			loggerPlusMoins.debug("Inversement du tableau utilisateur");
		}
    	this.nombreUtilisateur = renverseTableauxEntiers(this.nombreUtilisateur);
    }
    
    public void lancementOperation() {
    	if(getJoueur().getIdentifiant() == 1 && token)
    	{operationChiffreJoueur(getNombreSecret());} // Mode Challenger ou duel joueur

    	else if(getOrdinateur().getIdentifiant() == 2 && !token)
    	{	
    		operationChiffreOrdinateur(this.nombreUtilisateur);
    	}
    }
    
    public void modeChallenger() {
    	
		if(getJoueur().getTentative() > (getJoueur().getTentativeMaxPM()-1)) {
			getAffichage().tentativeMaxJoueur(getJoueur().getTentativeMaxMM());
			getAffichage().affichageEnteteSolution();
			getAffichage().affichageSolution(nombreSecret, getNombreChiffre());
			setSortieTentative(true);
			return;
		}
    	
		getAffichage().tentative(getJoueur().getTentative()+1);
		if(getJoueur().saisieValide(getNombreChiffre())) {
			reverseTabUtilisateur(getJoueur().getTmp());
			lancementOperation();
		}
		else {
			getAffichage().erreurLongueurCombinaison(getNombreChiffre());
			modeChallenger();
		}
	}
    

    public void modeDefenseur() {
    	token = false;
		if(getOrdinateur().getTentative() > getOrdinateur().getTentativeMaxPM()-1) {
			getAffichage().tentativeMaxAtteintes();
			getAffichage().affichageEnteteSolution();
			getAffichage().affichageSolution(nombreSecret, getNombreChiffre());
			setFinTraitement(true);
			return;
		}
    	//ValeurRetourFonction : Premier passage faux ensuite vrai pour traitement ordinateur     	
		if(!isValeurRetourFonction()) {
			ordinateurRechercheCombi = initTableauRecherche(getNombreChiffre());
			if(getJoueur().saisieValide(getNombreChiffre())) {
				getAffichage().affichageTentative(getOrdinateur().getTentative());
				reverseTabUtilisateur(getJoueur().getTmp());
				lancementOperation();
				setValeurRetourFonction(true);
			}
			else {
				getAffichage().erreurLongueurCombinaison(getNombreChiffre());
				modeDefenseur();
			}
		}
		else {
			getAffichage().affichageTentative(getOrdinateur().getTentative());
			reverseTabUtilisateur(getJoueur().getTmp());
			lancementOperation();
		}
		
    }
    
    public void modeDuel() {
    	
    	getAffichage().debutDuelPileOuFace();
		
		Boolean premierJoueur = startPremierJoueur();
		
		while(premierJoueur == null)
		{
			getAffichage().erreurSaisieSimple();
			premierJoueur = startPremierJoueur();
		}
		
		token = (premierJoueur == true);		
		
		while(!isFinTraitement())
		{
			if(token == true)
			{
				getJoueur().setTentativeIncr();
				getAffichage().tourJoueur();
				getAffichage().affichageTentative(getJoueur().getTentative());
				if(getJoueur().saisieValide(getNombreChiffre())) {
					reverseTabUtilisateur(getJoueur().getTmp());
					lancementOperation();
				}
				else {
					getAffichage().erreurLongueurCombinaison(getNombreChiffre());
					modeDuel();
				}

				
				if(isFinTraitement())
				{
					getAffichage().victoireJoueur(getJoueur().getTentative());
				}
				
				if(getJoueur().getTentative() > (getJoueur().getTentativeMaxPM()-1)) {
					getAffichage().tentativeMaxJoueur(getJoueur().getTentativeMaxMM());
					getAffichage().affichageEnteteSolution();
					getAffichage().affichageSolution(nombreSecret, getNombreChiffre());
					setSortieTentative(true);
					return;
				}
				token = false;
			}
			else 
			{	
				getOrdinateur().setTentativeIncr();
				getAffichage().tourOrdinateur();
				getAffichage().affichageTentative(getOrdinateur().getTentative());
				operationChiffreOrdinateur(getNombreSecret());
				if(isFinTraitement())
				{
					getAffichage().victoireOrdinateur(getOrdinateur().getTentative());
				}

				if(getOrdinateur().getTentative() > getOrdinateur().getTentativeMaxPM()-1) {
					getAffichage().tentativeMaxAtteintes();
					getAffichage().affichageEnteteSolution();
					getAffichage().affichageSolution(nombreSecret, getNombreChiffre());
					setFinTraitement(true);
					return;
				}
				token = true;
			}
		}
    }
    
    protected void operationChiffreJoueur(long nombreR[])
    {
    	int chiffreValide = 0;
		if(isModeDev() && isTokenDebug()) {
			loggerPlusMoins.debug("Traitement de la proposition du joueur et affichage de la correction");
		}
			for(int i = 0; i < getNombreChiffre(); i++)
			{
				if(nombreR[i] == this.nombreUtilisateur[i])
				{
					System.out.print("=");
					chiffreValide++;
				}
				else if(nombreR[i] < this.nombreUtilisateur[i])
				{
					System.out.print("-");
				}
				else if(nombreR[i] > this.nombreUtilisateur[i])
				{
					System.out.print("+");
				}
			}
			System.out.println("");
			this.setValeurChiffreOk(chiffreValide);
			this.verificationFinTraitement();    	
    }
    
    private void operationChiffreOrdinateur(long[] nombreUtilisateur2)
    {
    	int chiffreValide = 0; 
		if(isModeDev() && isTokenDebug()) {
			loggerPlusMoins.debug("Traitement de la proposition de l'ordintaeur et affchage de la correction");
		}
		for(int i = 0; i < getNombreChiffre(); i++)
		{
		    if(nombreUtilisateur2[i] == this.ordinateurRechercheCombi[i])
		    {
		    	chiffreValide++;
		    	System.out.print(this.ordinateurRechercheCombi[i] + " ==> = ");
		    }
		    else if(nombreUtilisateur2[i] < this.ordinateurRechercheCombi[i])
		    {	
		    	System.out.print(this.ordinateurRechercheCombi[i] + " ==> - ");
		    	this.ordinateurRechercheCombi[i] -= 1;
		    }
		    else if(nombreUtilisateur2[i] > this.ordinateurRechercheCombi[i])
		    {
		    	System.out.print(this.ordinateurRechercheCombi[i] + " ==> + ");
		    	this.ordinateurRechercheCombi[i] += 1;
		    }
		  }
		System.out.println("");
		this.setValeurChiffreOk(chiffreValide);
		this.verificationFinTraitement();
	 }
    
	protected void verificationFinTraitement() {
		if(valeurChiffreOk == getNombreChiffre()) {
			setFinTraitement(true);
		}
		else
			setFinTraitement(false);
	}


	protected void setValeurChiffreOk(int valeurChiffreOk) {
		this.valeurChiffreOk = valeurChiffreOk;
	}


	@Override
	protected AffichagePlusMoins getAffichage() {
		return affichage;
	}
	
	protected long[] getNombreSecret() {
		return nombreSecret;
	}

	protected void setNombreSecret(long[] nombreSecret) {
		this.nombreSecret = nombreSecret;
	}

	protected long getLongueurNombreUtilisateur() {
		return longueurNombreUtilisateur;
	}

	protected void setLongueurNombreUtilisateur(long longueurNombreUtilisateur) {
		this.longueurNombreUtilisateur = longueurNombreUtilisateur;
	}

	protected boolean isValeurRetourFonction() {
		return valeurRetourFonction;
	}

	protected void setValeurRetourFonction(boolean valeurRetourFonction) {
		this.valeurRetourFonction = valeurRetourFonction;
	}

	public long getTmp() {
		return tmp;
	}

	public void setTmp(long tmp) {
		this.tmp = tmp;
	}
}
