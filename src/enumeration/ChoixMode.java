package enumeration;

public enum ChoixMode {

	MODECHALLENGER(1),
	MODEDEFENSEUR(2),
	MODEDUEL(3),
	RETOUR(0);
	
	private int mode = 0;
	
	ChoixMode(int mode)
	{
		this.mode = mode;
	}
	
	public int getMode() {
		return this.mode;
	}

	public static ChoixMode modeDeJeu(long saisieUtilisateur)
	{
		for(ChoixMode test : ChoixMode.values()){
		      if(test.getMode() == saisieUtilisateur)
		        return test;
		    }
		return null;
	}
}
