package enumeration;

public enum ChoixJeu {
	
	PLUSOUMOINS(1),
	MASTERMIND(2),
	QUITTER(0);

	private int jeu = 0;
	
	ChoixJeu(int jeu)
	{
		this.jeu = jeu;
	}
	
	public int getJeu() {
		return this.jeu;
	}
	
	public static ChoixJeu jeu(long saisieUtilisateur)
	{
		for(ChoixJeu test : ChoixJeu.values()){
		      if(test.getJeu() == saisieUtilisateur)
		        return test;
		    }
		return null;
	}
}