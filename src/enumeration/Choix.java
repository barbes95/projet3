package enumeration;
import java.util.Random;

public enum Choix {

	PILE ("PILE", "P"),
	FACE ("FACE", "F");
	
	private  String choixComplet = "";
	private  String choixInitial = "";
	

	  //Constructeur
	  Choix(String choixComplet, String choixInitial){
	    this.choixComplet = choixComplet;
	    this.choixInitial = choixInitial;
	  }
	   
	  public String getChoixComplet() {
		  return this.choixComplet;
	  }
	  
	  public String getChoixInitial() {
		  return this.choixInitial;
	  }
	  
	  /**
	   * Retourne vrai si le choix correspond à PILE
	   * @param choix
	   * @return
	   */
	  private static boolean isEqualStringPile(String choix) 
	  {
		  return Choix.isEqualString(choix, PILE);
	  }
	  
	  /**
	   * Retourne vrai si le choix correspond à FACE
	   * @param choix
	   * @return
	   */
	  private static boolean isEqualStringFace(String choix)
	  {
		  return Choix.isEqualString(choix, FACE);
	  }
	  
	  private static boolean isEqualString(String choix, Choix test)
	  {
		  return test.getChoixComplet().equals(choix.toUpperCase()) 
				  || test.getChoixInitial().equals(choix.toUpperCase());
	  }
	  
	  public static Boolean isPlayerStart(String choix)
	  {
		  if(!isEqualStringFace(choix) && !isEqualStringPile(choix))
		  {
			  return null;
		  }
		  
		  Random rand = new Random();
		  if(rand.nextBoolean())
		  {
			  return Choix.isEqualStringPile(choix); 
		  }
		  else
		  {
			  return Choix.isEqualStringFace(choix);
		  }
	  }
	  
	  
}
