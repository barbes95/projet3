package model;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import services.ChargementFichier;

public class Joueur {

	private int tentative = 0;

	private static int tentativeMaxMM;
	private static int tentativeMaxPM;
	private int identifiant; 
	private static Scanner entreeUtilisateurString = new Scanner(System.in);
	private boolean victoire;
	private long tmp;
	static ChargementFichier chargementProprietes = new ChargementFichier();

	private Scanner scantest;

	public Joueur()
	{
		setIdentifiant(0);
		victoire = false;
	}

	/**
	 * @return the tentative
	 */
	public int getTentative() {
		return tentative;
	}

	/**
	 * @param tentative the tentative to set
	 */
	public void setTentative(int tentative) {
		this.tentative = tentative;
	}
	public void setTentativeIncr() {
		this.tentative++;
	}

	public boolean saisieValide(int nombreChiffre) {
		long entreeUtilisateur = -1;
		double longueur = -1;

		while(entreeUtilisateur == -1) {
			entreeUtilisateur = entreeUtilisateur();
			longueur = Math.log10(entreeUtilisateur) + 1;
		}
		if((int)longueur != nombreChiffre) {
			return false;
		}
		else
			setTmp(entreeUtilisateur);
		return true;
	}

	public long entreeUtilisateur()
	{
		scantest = new Scanner(System.in);

		try {
			long temp = scantest.nextLong();
			scantest.nextLine();
			return temp;
		}
		catch (InputMismatchException e) {
			return -1;
		}
		catch(NoSuchElementException e)
		{
			return -1;
		}
	}

	public String entreeUtilisateurString()
	{
		try {
			//entreeUtilisateur.nextLine();
			return entreeUtilisateurString.nextLine();
		}
		catch(InputMismatchException e){
			return "erreur de saisie";
		}
	}

	/**
	 * @return the identifiant
	 */
	public int getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}

	protected boolean isVictoire() {
		return victoire;
	}

	protected void setVictoire(boolean victoire) {
		this.victoire = victoire;
	}

	public void setTentativeMaxMM(String cle) {
		chargementProprietes.properties(cle);
		tentativeMaxMM = Integer.parseInt(chargementProprietes.getValeurCle());
	}

	public int getTentativeMaxMM() {
		return tentativeMaxMM;
	}

	public long getTmp() {
		return tmp;
	}

	public void setTmp(long tmp) {
		this.tmp = tmp;
	}

	public int getTentativeMaxPM() {
		return tentativeMaxPM;
	}

	public void setTentativeMaxPM(String cle) {
		chargementProprietes.properties(cle);
		tentativeMaxPM = Integer.parseInt(chargementProprietes.getValeurCle());
	}
}

