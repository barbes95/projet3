package model;
import controllers.ScenarioJeu;


public class Launch {

	private static ScenarioJeu deroulement = new ScenarioJeu();
	
	public static void main(String[] args) {
		

		boolean continuerJeu = true;
		int debug = 0;

		debug = Integer.parseInt(args[0]);

		while(continuerJeu)
		{
			continuerJeu = deroulement.choixJeuUtilisateur(debug);
		}
	}    
}
