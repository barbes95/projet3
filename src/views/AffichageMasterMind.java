package views;

public class AffichageMasterMind extends AffichageJeu {

	void modeChallenger(){
		System.out.println("MasterMind : Mode Challenger");
	}
	
	void modeDefenseur() {
		System.out.println("MasterMind : Mode défenseur");
	}
	
	void modeDuel() {
		System.out.println("MasterMind : Mode Duel");
	}
	
	public void challengerPlace(int place) {
		System.out.println(place + " chiffre bien placé");
	}
	
	public void challengerPresent(int present) {
		System.out.println(present + " "
			+ ""
			+ "chiffres présent mais mal placés");
	}
	
	public void tentative(int tenta) {
		System.out.println("Tentative " + tenta);
	}
	
	public void affichageWinOrdinateur() {
		System.out.print("L'ordinateur a trouvé la combinaison secréte : ");
	}
}
