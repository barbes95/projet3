package views;

public class AffichageUtilisateur {
	
	public void menuJeu() {
		System.out.println("Veuillez choisir votre jeu :");
		System.out.println("1 - Plus ou Moins");
		System.out.println("2 - MasterMind");
		System.out.println("0 - Quitter le jeu");
	}
	
	public void erreurSaisie()
	{
		System.out.println("Veuillez saisir une valeur correcte");
	}
	
	public void tentative(int tenta) {
		System.out.println("Tentative " + tenta);
	}
	
	public void victoireJoueur(int tentative) {
		System.out.println("Vous avez gagné en " + tentative + " tentatives");
	}
	
	public void victoireOrdinateur(int tentative) {
		System.out.println("L'ordinateur a gagné en " + tentative + " tentatives");
	}
}
