package views;

public abstract class AffichageJeu extends AffichageUtilisateur {
	
	abstract void modeChallenger();
	abstract void modeDefenseur();
	abstract void modeDuel();
	
	
	public void menuModeJeu() {
		System.out.println("Veuillez choisir votre mode de jeu :");
		System.out.println("1 - Mode Challenger");
		System.out.println("2 - Mode Defenseur");
		System.out.println("3 - Mode Duel");
		System.out.println("0 - Revenir au choix des jeux");
	}
	
	public void saisieCombinaison(long l)
	{
		System.out.println();
		System.out.println("Veuillez saisir une combinaison de " + l + " chiffres");
	}
	
	public void affichageNombreTentative(int tentative)
	{
		System.out.println("La combinaison secréte a été trouvée en "+ tentative + " tentatives");
	}
	
	public void affichageTentative(int tentative)
	{
		System.out.println("Tentative " + tentative);
	}
	
	public void debutDuelPileOuFace()
	{
		System.out.println("Choisissez pile ou face pour savoir qui commence ('p' ou 'pile / 'f' ou 'face') : ");
	}
	
	public void erreurSaisieSimple()
	{
		System.out.println("Veuillez saisir une entrée correcte");
	}
	
	public void tourJoueur()
	{
		System.out.println();
		System.out.println("Tour du joueur :");
	}
	
	public void tourOrdinateur() {
		System.out.println();
		System.out.println("Tour de l'ordinateur :");
	}
	
	public void erreurLongueurCombinaison(int longueurCombinaison) {
		System.out.println("Erreur, veuillez saisir une combinaison de " + longueurCombinaison + " chiffres");
	}
	
	public void tentativeMaxJoueur(int tentativeMax) {
		System.out.println("Perdu ! Vous n'avez pas trouvé la combinaison avant les " + tentativeMax + " tentatives maximum");
	}
	
	public void affichageEnteteSolution() {
		System.out.println("La combinaison secréte était : ");
	}
	
	public void affichageSolution(long [] tab, int longueur) {
		for(int i = 0; i < longueur; i++) {
			System.out.print(tab[i]);
		}
		System.out.println();
	}
	
	public void tentativeMaxAtteintes() {
		System.out.println("L'ordinateur n'a pas trouvé la combinaison secrête");
	}
}
